
<div class="footer">
    <div class="title_sub">
        <p class="titlef"><span class="ledf">L'ECOLE DU</span> NUMERIQUE</p>
        <p class="subf">Formation labellisées : Simplon / Campus Région du Numérique</p>
    </div>

    <div class="logosocial">
        <a href="https://www.facebook.com/campus26formation"><i class="fab fa-facebook-square fa-lg"></i></a>
        <a href="https://twitter.com/Campus_26"><i class="fab fa-twitter fa-lg"></i></a>
        <a href="https://www.linkedin.com/company/campus26/"><i class="fab fa-linkedin fa-lg"></i></a>
    </div>

    <div class="contactus">
        <div class="tata">
            <p class="contp">Si vous avez des questions,</p>
            <p class="cont">CONTACTEZ-NOUS</p>
        </div>
        <div class="tete">
            <button id="btn3" class="contbtn">CLIQUEZ-ICI</button>
        </div>
    </div>

    <div class="footer_column">
        <div class="column1">
            <h4 class="columntitle">CONTACT</h4>
            <p class="columnp"><i class="fas fa-phone"></i>04 15 49 00 03</p>
            <p class="columnp"><i class="fas fa-map-marker-alt"></i>4 rue du PNDF 43000 Le Puy-en-Velay</p>
            <p class="columnp"><i class="fas fa-laptop"></i>Campus26.Com</p>
            <p class="columnp"><i class="fas fa-graduation-cap"></i>Accessibilité</p>
        </div>
        <div class="column1">
            <h4 class="columntitle">NOTRE SITE</h4>
            <a href="index.html">
                <p class="columnp">Accueil</p>
            </a>
            <a href="https://www.lecoledunumerique.fr/le-projet/">
                <p class="columnp">Le projet</p>
            </a>
            <a href="https://www.lecoledunumerique.fr/les-parcours/">
                <p class="columnp">Les parcours</p>
            </a>
            <a href="https://www.lecoledunumerique.fr/candidater/">
                <p class="columnp">Candidater</p>
            </a>
            <a href="https://www.lecoledunumerique.fr/blog/">
                <p class="columnp">Blog</p>
            </a>
            <a href="https://www.lecoledunumerique.fr/politique-de-confidentialite/">
                <p class="columnp">Politique de confidentialité</p>
            </a>
            <a href="https://www.lecoledunumerique.fr/plan-du-site/">
                <p class="columnp">Plan du site</p>
            </a>
            <a href="https://www.lecoledunumerique.fr/mentions-legales/">
                <p class="columnp">Mentions légales</p>
            </a>
            <a href="https://www.lecoledunumerique.fr/blog/">
                <p class="columnp">Blog</p>
            </a>
        </div>
        <div class="column1">
            <h4 class="columntitle">NOS PARTENAIRES</h4>
            <a href="https://www.campus26.com/">
                <p class="columnp">Campus26</p>
            </a>
            <a href="https://simplon.co/">
                <p class="columnp">Simplon</p>
            </a>
            <a href="https://www.auvergnerhonealpes.fr/">
                <p class="columnp">Région Auvergne-Rhône-Alpes</p>
            </a>
            <a href="https://www.agglo-lepuyenvelay.fr/">
                <p class="columnp">L'agglo Le Puy-en-Velay</p>
            </a>
            <a href="https://www.pole-emploi.fr/accueil/">
                <p class="columnp">Pôle emploi</p>
            </a>
            <a href="https://www.labrasseriedudigital.com/">
                <p class="columnp">La brasserie du digital</p>
            </a>
        </div>
    </div>

    <div class="corp">
        <h4 class="corptitle">© 2020 CAMPUS26.</h4>
    </div>

    <div class="ptiteligne">
        <p class="ptitelignep">Ce site est protégé par reCAPTCHA. Les <a href="https://policies.google.com/privacy?hl=fr"><span class="ptitelignesp">règles de confidentialité</span></a> et les <a href="https://policies.google.com/terms?hl=fr"><span class="ptitelignesp">conditions d’utilisation</span></a> de Google s’appliquent.</p>
    </div>

</div>







</body>

</html>