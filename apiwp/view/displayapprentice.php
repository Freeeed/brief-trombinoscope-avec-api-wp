<!-- page qui chope ce que je veux récup dans le json -->

<form method="post" action="">
    <div class="search">
        <select class="dropdown" name="promo_year" value="0">
            <option value="0">Selectionner une promotion</option>
            <?php foreach ($promotions as $value) : ?>
                <option value="<?= $value->id; ?>"><?= $value->name; ?></option>
            <?php endforeach; ?>
        </select>
        <div class="searchbar">
            <input type="text" placeholder="Rechercher...">
        </div>
        <div class="skill_and_btn">
            <div class="skillfilter">
                <?php foreach ($competences as $value) : ?>
                    <div><input type="checkbox"><label><?= $value->name; ?></label></div>
                <?php endforeach; ?>
            </div>
            <div class="form_btn"><button>Valider</button></div>
        </div>
    </div>
</form>

<div class="card-container">
    <div class="card-grid">
        <?php foreach ($data as $value) : ?>
            <div class="template-card">
                <img src="<?= $value->acf[3]; ?>" alt="">
                <h2><?= $value->title->rendered; ?></h2>
                <p><?= $value->excerpt->rendered; ?></p>
                <div class="card-icons">
                    <a href="<?= $value->acf[0]; ?>" target="_blank"><i class="fab fa-linkedin fa-lg"></i></a>
                    <a href="<?= $value->acf[1]; ?>" target="_blank"><i class="fas fa-address-book fa-lg"></i></a>
                    <a href="<?= $value->acf[2]; ?>" target="_blank"><i class="fas fa-file-download fa-lg"></i></a>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<!-- < ? = permet d'ouvrir du php et d'effectuer un echo en meme temps -->
<!-- foreach = pour chaque $value du tableau [$data] -->