<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css"/>
    <link rel="stylesheet" href="./style.css">
    <title>api apprenants</title>
</head>
<body>
    
<header>
    <nav>
        <a href="index.html"><img class="logo" src="img/LogoednB-e1606908793400-2048x1255.png" alt=""></a>
        <ul class="header_ul">
            <li class="header_li"><a class="header_li" href="">Le projet</a></li>
            <li class="header_li">Initiation à l'informatique</li>
            <li class="header_li">Devenir développeur</li>
            <li class="header_li">Candidater</li>
            <li class="header_li">Blog</li>
            <li><i id="myBtn" class="far fa-comments fa-lg"></i></li>
        </ul>
    </nav>
</header>

<section class="intro">
    <div class="title">
        <h3 class="pev">AU PUY-EN-VELAY</h3>
        <h1 class="num"><span class="led">L'ECOLE DU</span> NUMERIQUE</h1>
        <div class="bycamp">
            <h3 class="byc">By Campus26</h3>
        </div>
        <h3 class="trombi">TROMBINOSCOPE</h3>
    </div>
</section>