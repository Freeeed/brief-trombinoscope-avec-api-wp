
<?php

class ApprenticeController
{

    private $templateEngine;
    private $model;

    public function __construct($templateEngine, $model)
    {
        $this->templateEngine = $templateEngine;
        $this->model = $model;
    }

    public function displayList()
    {
        if (isset($_POST["promo_year"])) {
            if ($_POST["promo_year"] === '0') {
                $data = $this->model->getApprenticeData("http://localhost/briefapiwordpress/wp-json/wp/v2/apprenant?per_page=100");
            }
            if ($_POST["promo_year"] === '14') {
                $data = $this->model->getApprenticeData("http://localhost/briefapiwordpress/wp-json/wp/v2/apprenant?promotion=14");
            }
            if ($_POST["promo_year"] === '15') {
                $data = $this->model->getApprenticeData("http://localhost/briefapiwordpress/wp-json/wp/v2/apprenant?promotion=15");
            }
        } else {
            $data = $data = $this->model->getApprenticeData("http://localhost/briefapiwordpress/wp-json/wp/v2/apprenant?per_page=100");
        }
        $promotions = $this->model->getApprenticeData("http://localhost/briefapiwordpress/wp-json/wp/v2/promotion");
        $competences = $this->model->getApprenticeData("http://localhost/briefapiwordpress/wp-json/wp/v2/competences");
        return $this->templateEngine->render('displayapprentice.php', array('data' => $data, 'promotions' => $promotions, 'competences' => $competences));
    }
}

// if ($_POST["promo_year"] === '0') va chercher dans displayapprentice le name "promo_year" et comme la valeur est égale a 0 
// alors 